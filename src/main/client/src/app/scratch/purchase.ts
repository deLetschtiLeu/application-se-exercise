export class Purchase {

  public id: number;
  public name: string;
  public product_name: string;
  public date: string;
  public price: string;

    constructor(id: number, name: string, product_name: string, date: string, price: string) {
        this.id = id;
        this.name = name;
        this.product_name = product_name;
        this.date = date;
        this.price = price;
    }



}
