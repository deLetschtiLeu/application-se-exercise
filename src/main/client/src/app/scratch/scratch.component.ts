import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {User} from './user';
import {Purchase} from './purchase';

@Component({
  selector: 'app-scratch',
  templateUrl: './scratch.component.html',
  styleUrls: ['./scratch.component.css']
})
export class ScratchComponent implements OnInit {

  purchase: Purchase;

  purchases: String[];


  constructor( private httpClient:HttpClient) { }

  ngOnInit() {
  }



  // get all purchases
    getPurchases() {
    this.httpClient.get<String[]>("api/purchases")
    // NOTE: ideally, we should have an error handler here, which we left away for simplicity
      .subscribe(resp => {
        this.purchases = resp;
      });

  }


  savePurchaseClick(name, product_name, date, price) {
      if((typeof name!='undefined' && name) && (typeof product_name!='undefined' && product_name) && (typeof date!='undefined' && date) && (typeof price!='undefined' && price)){
            let purch =    '{ "name": ' + name + ' , "product_name":' + product_name + 
                    ' , "date":' + date +  ' , "price":' + price + ' }';
            this.httpClient.post("api/savepurchase", purch).subscribe(resp => {});
      } else {
        alert('Bitte fülle alle Felder aus');
      }
    }


}
