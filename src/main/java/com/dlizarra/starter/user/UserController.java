package com.dlizarra.starter.user;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dlizarra.starter.role.RoleName;

@RestController
public class UserController {
	@Autowired
	private UserService userService;
	
	private List<String> purchases = new ArrayList<String>();

	@RequestMapping(value = "/users", method = RequestMethod.GET)
	public List<UserDto> findAll() {
		return userService.getUsers();
	}
	
	@RequestMapping(value = "/purchases", method = RequestMethod.GET)
	public List<String> getPurchases() {
		return purchases;
	} 
	
	//get the Post request for purchases
	@RequestMapping(value = "/savepurchase", method = RequestMethod.POST)
	public void savePurchase(HttpServletRequest request) {
		String post_body = extractPostRequestBody(request);
		System.out.println(post_body);
		this.purchases.add(post_body);
	} 
	
	
	

	//To extract the post request body
	static String extractPostRequestBody(HttpServletRequest request) {
        Scanner s = null;
        try {
            s = new Scanner(request.getInputStream(), "UTF-8").useDelimiter("\\A");
        } catch (IOException e) {
            e.printStackTrace();
        }
        String res = s.hasNext() ? s.next() : "";
        
        return res;
	}
	
}
